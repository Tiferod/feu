using namespace std;
#include <iostream>
#include <string>
#include <windows.h>
#include <time.h>
#include "feu.h"
#include "route.h"


int main()
{
	srand(time(NULL));
	CRoute route;
    route.AddFeu(CFeu(10, new CVert()));
    route.AddFeu(CFeu(10, new CVert()));
    route.AddFeu(CFeu(10, new CVert()));
    route.AddFeu(CFeu(10, new CVert()));
	while (!GetAsyncKeyState(VK_F4))
	{
		route.Display();
		Sleep(100);
	}
	return 0;
}

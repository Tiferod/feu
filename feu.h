#pragma once
class CFeu;
class CState
{
public:
	CState() {};
	string name;
	virtual int run(int& nb) = 0;
	virtual void Vert(CFeu* feu) {};
	virtual void Orange(CFeu* feu) {};
	virtual void Rouge(CFeu* feu) {};
};
class CFeu
{
private:
	int nombrevoitures;
	CState* State;
public:
	CFeu(int nbvoitures, CState* state) { nombrevoitures = nbvoitures;State = state; }
	void AddVoiture() { nombrevoitures ++; }
	void SetState(CState* state) { State = state; }
	CState* GetState() { return State; }
	int run()
	{
		cout << "Nombre de voitures : " << nombrevoitures << " // " << State->name;
		int change= State->run(nombrevoitures);
		if (nombrevoitures < 0)
		{
			if (abs(nombrevoitures) < change)
			{
				change = abs(nombrevoitures);
				nombrevoitures = 0;
			}
			else
			{
				change = 0;
				nombrevoitures = 0;
			}
		}
		return change;
	}
};
class CRouge:public CState
{
public:
	CRouge() { name = "feu rouge"; };
	int run(int& nb)
	{
		return 0;
	}
	void Vert(CFeu* feu);
};
class COrange:public CState
{
public:
	COrange() { name = "feu orange"; };
	int run(int& nb)
	{
		nb -= 1;
		return 1;
	}
	void Rouge(CFeu* feu)
	{
		feu->SetState(new CRouge());
		delete this;
	}
};
class CVert :public CState
{
public:
	CVert() { name = "feu vert"; };
	int run(int& nb)
	{
		nb-= 2;
		return 2;
	}
	void Orange(CFeu* feu)
	{
		feu->SetState(new COrange());
		delete this;
	}
};
void CRouge::Vert(CFeu* feu)
{
	feu->SetState(new CVert());
	delete this;
}

#pragma once
#include <vector>
#include "feu.h"
class CRoute
{
public:
	vector <CFeu> feux;
	CRoute() {};
	int selection;
	void AddFeu(CFeu feu) { feux.push_back(feu); }
	void Display()
	{
		system("CLS");
		cout << "Gestion de l'intersection :\n-------------------------\n\n";
		for (int i = 0;i < feux.size();i++)
		{
			int distrib = feux[i].run();
			while (distrib)
			{
				int newvoie = rand() % feux.size();
				if (newvoie != i)
				{
					feux[newvoie].AddVoiture();
					distrib--;
				}
			}
			if (i == selection)
			{
				cout << " <--";
				if (GetAsyncKeyState(VK_F1))feux[i].GetState()->Rouge(&feux[i]);
				if (GetAsyncKeyState(VK_F2))feux[i].GetState()->Orange(&feux[i]);
				if (GetAsyncKeyState(VK_F3))feux[i].GetState()->Vert(&feux[i]);
			}
			cout << endl;
		}
		if (feux.size())
		{
			cout << "\nF1: passer au rouge\nF2: passer au orange\nF3: passer au vert";
			if (GetAsyncKeyState(VK_UP))
			{
				selection--;
				if (selection < 0)selection = 0;
			}
			if (GetAsyncKeyState(VK_DOWN))
			{
				selection++;
				if (selection > feux.size()-1)selection = feux.size() - 1;
			}
		}
	}
};